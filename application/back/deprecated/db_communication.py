import psycopg2
import json
import utility_functions

DB_DATA_PATH = "../db connection data/"


def get_connection():
    db_conn_data = None
    conn = None
    with open("../db connection data/db_settings.json", "r") as db_conn_file:
        db_conn_data = json.load(db_conn_file)
    try:
        connection_string = (
            "dbname='" + str(db_conn_data['dbname']) +
            "' user='" + str(db_conn_data['user']['name']) +
            "' password='" + db_conn_data['user']['password'] +
            "' host='" + db_conn_data['host'] + "'"
        )
        conn = psycopg2.connect(connection_string)
    except Exception:
        conn = None
    return conn


def create_db_tables():
    conn = get_connection()
    cursor = conn.cursor()
    db_signature = None
    sql = (
        "DROP SCHEMA public CASCADE;" +
        "CREATE SCHEMA public;" +
        "GRANT ALL ON SCHEMA public TO postgres;" +
        "GRANT ALL ON SCHEMA public TO public;"
    )
    cursor.execute(sql)

    with open(DB_DATA_PATH + "db_signature.json", "r") as signature_file:
        db_signature = json.load(signature_file)

    for key, value in db_signature['types']['enums'].items():
        sql = "CREATE TYPE " + key + " AS ENUM ("
        for index in range(0, len(value) - 1):
            sql += "'" + value[index] + "',"
        sql += "'" + value[-1] + "');"
        cursor.execute(sql)
    for key, value in db_signature['tables'].items():
        sql = "CREATE TABLE " + key + "("
        table_data = list(value['columns'].items())
        for index in range(0, len(table_data) - 1):
            sql += table_data[index][0] + " " + table_data[index][1] + ","
        sql += table_data[-1][0] + " " + table_data[-1][1]
        if len(value['primary_key']) != 0:
            sql += ",PRIMARY KEY("
            for index in range(0, len(value['primary_key']) - 1):
                sql += value['primary_key'][index] + ","
            sql += value['primary_key'][-1] + ")"
        sql += ");"
        print(sql)
        cursor.execute(sql)
    for key, value in db_signature['tables'].items():
        if len(value['foreign_key']) != 0:
            for val in value['foreign_key']:
                sql = (
                    "ALTER TABLE " + key +
                    " ADD CONSTRAINT fk_" +
                    key + "_" + val['table'] + " " +
                    " FOREIGN KEY(" +
                    val['column'] +
                    ") REFERENCES " +
                    val['table'] + ";"
                )
            print(sql)
            cursor.execute(sql)
    conn.commit()
    conn.close()


def add_employee(data):
    conn = get_connection()
    cursor = conn.cursor()
    pass_data = utility_functions.get_hashed_password(data['password'])
    sql = (
        "INSERT INTO employee (email_id, name, password, salt, job) VALUES (" +
        data['email_id'] + "," +
        data['name'] + "," +
        pass_data[0] + "," +
        pass_data[1] + "," +
        data['job'] + ");"
    )
    cursor.execute(sql)
    conn.commit()
    conn.close()


def add_workflow(data):
    conn = get_connection()
    cursor = conn.cursor()
    sql = (
        "INSERT INTO workflow (user_id, name, original, " +
        "petri_representation) VALUES " +
        data['user_id'] + "," +
        data['name'] + "," +
        data['original'] + "," +
        data['petri_representation'] + ")"
    )
    cursor.execute(sql)
    conn.commit()
    conn.close()


def add_task(data):
    conn = get_connection()
    cursor = conn.cursor()
    sql = (
        "INSERT INTO task (user_id, workflow_id, title, " +
        "description, resources) VALUES " +
        data['user_id'] + "," +
        data['workflow_id'] + "," +
        data['title'] + "," +
        data['description'] + "," +
        data['resources'] + ")"
    )
    cursor.execute(sql)
    conn.commit()
    conn.close()


def get_employee_data(user_id, email_id=None):
    conn = get_connection()
    cursor = conn.cursor()
    sql = "SELECT id, email_id, name, password, salt, job FROM employee WHERE "
    if email_id is not None:
        sql += "email_id='" + email_id + "'"
    else:
        sql += "user_id=" + user_id
    sql += ";"
    cursor.execute(sql)
    employee = cursor.fetchall()
    data = {'id': employee[0], 'name': employee[1]}
    return data
