import convert_workflow
import json
from workflow_data import WorkflowData, WorkflowArch
from petri_net import PetriNet

# workflow = json.load(open("../workflow definitions/rares.stan/New Network 3 - 3.json", "r"))
# json.dump(convert_workflow.convert_to_petri_representation(workflow['workflow']), open("test.json", "w"), indent=4, sort_keys=True)

data = WorkflowData()
workflow = json.load(open("test.json", "r"))
for loc in workflow['nodes']:
    if loc['id'] == "in":
        data.set_input(loc['id'])
    elif loc['id'] == "out":
        data.set_output(loc['id'])
    else:
        data.add_location(loc['id'])

for tran in workflow['transitions']:
    data.add_transition(tran['id'])

for arch in workflow['edges']:
    data.add_arch(WorkflowArch(arch['from'], arch['to']))

petri_net = PetriNet(data)

SCC = petri_net._strongly_connected_components()
interiorSCC = petri_net._interior_strongly_connected_components(SCC)
petri_net.soundness()

# print(SCC)
