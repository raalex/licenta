from workflow_data import WorkflowData, WorkflowArch
from petri_net import PetriNet

workflow_data = WorkflowData()
workflow_data.set_input("p1")
workflow_data.set_output("p3")
workflow_data.add_location("p2")

workflow_data.add_transition("t1")
workflow_data.add_transition("t2")
workflow_data.add_transition("t3")
workflow_data.add_transition("t4")

workflow_data.add_arch(WorkflowArch("p1", "t1", 1))
workflow_data.add_arch(WorkflowArch("t1", "p3", 1))
workflow_data.add_arch(WorkflowArch("t4", "p1", 1))
workflow_data.add_arch(WorkflowArch("p3", "t4", 1))

workflow_data.add_arch(WorkflowArch("t3", "p1", 1))
workflow_data.add_arch(WorkflowArch("p1", "t3", 1))
workflow_data.add_arch(WorkflowArch("t3", "p2", 1))
workflow_data.add_arch(WorkflowArch("p2", "t2", 1))

workflow_data.add_arch(WorkflowArch("t2", "p3", 1))
workflow_data.add_arch(WorkflowArch("p3", "t2", 1))


petri_net = PetriNet(workflow_data)

print(petri_net)
