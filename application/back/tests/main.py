from workflow_data import WorkflowData, WorkflowArch
from petri_net import PetriNet


workflow_data = WorkflowData()

f = open("test2.txt", "r")

locations = f.readline().strip("\n").split(" ")
workflow_data.set_input(locations[0])
workflow_data.set_output(locations[-1])
for loc in locations[1:-1]:
    workflow_data.add_location(loc)

for tran in f.readline().strip("\n").split(" "):
    workflow_data.add_transition(tran)

for arch in f.readlines():
    nodes = arch.strip("\n").split(" ")
    workflow_data.add_arch(WorkflowArch(nodes[0], nodes[1], 1))

petri_net = PetriNet(workflow_data)

# print(petri_net)

SCC = petri_net._strongly_connected_components()
interiorSCC = petri_net._interior_strongly_connected_components(SCC)
petri_net._soundness()

print(SCC)
