import psycopg2
import json
from db_model import db, Employee, Workflow
import utility_functions

db.drop_all()
db.create_all()

emp1 = Employee("rares.stan", "Rares Stan", "123456", "developer")
emp2 = Employee("b.a", "B A", "123456", "manager")
db.session.add(emp1)
db.session.add(emp2)
db.session.commit()
# print(db_orm.Workflow.query.filter_by(employee_id=1).all())
# print(utility_functions.get_workflows_name_and_id(1))
