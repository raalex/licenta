from flask import Flask
from flask_restful import Api, Resource, reqparse
import db_model
import utility_functions
import petri_net


app = Flask(__name__)
api = Api(app)
app.config['SQLALCHEMY_DATABASE_URI'] = (
    'postgresql+psycopg2://' +
    'alex:lespaul95@localhost:5432' +
    '/workflow_licenta'
)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db_model.db.init_app(app)

parser = reqparse.RequestParser()
parser.add_argument("workflow", type=dict)


class WorkflowList(Resource):
    def get(self, user_id):
        workflows = utility_functions.get_workflows_name_and_id(int(user_id))
        # print(workflows)
        return {
            'workflows': workflows
        }

    def post(self, user_id):
        workflow_number = utility_functions.get_new_workflow_id(int(user_id))
        workflow_name = "New Network " + str(workflow_number)
        workflow_id = utility_functions.create_new_workflow(
            user_id,
            workflow_name
        )
        return {
            'id': workflow_id,
            'name': workflow_name,
            'workflow': utility_functions.blank_workflow,
            'user_type': utility_functions.get_user_types(),
            'state': False
        }


class WorkflowItem(Resource):
    def get(self, user_id, workflow_id):
        return utility_functions.get_workflow_json(workflow_id)

    def post(self, user_id, workflow_id):
        workflow_data = parser.parse_args()['workflow']
        # print(workflow_data)
        try:
            utility_functions.update_workflow(
                workflow_id,
                workflow_data['name'],
                workflow_data['workflow']
            )
        except petri_net.NotPseudoViable as e:
            workflow = utility_functions.get_workflow(workflow_id)
            msg = "Actions:"
            err = str(e).split(" ")
            for tran in err:
                for node in workflow.original['nodes']:
                    if str(node['id']) == tran:
                        msg += " " + node['label']
                        break
            msg += " cannot be executed"
            return {
                'error': msg
            }
        except Exception as e:
            return {
                'error': str(e)
            }

    def delete(self, user_id, workflow_id):
        utility_functions.delete_workflow(workflow_id)


class WorkflowStart(Resource):
    def post(self, user_id, workflow_id):
        try:
            utility_functions.start_workflow(workflow_id)
            workflow = utility_functions.get_workflow(workflow_id)

            return utility_functions.get_workflow_json(workflow_id)
        except Exception as e:
            return {
                'error': str(e)
            }


class WorkflowInterrupt(Resource):
    def post(self, user_id, workflow_id):
        utility_functions.interrupt_workflow(workflow_id)
        return utility_functions.get_workflow_json(workflow_id)


class SelectAction(Resource):
    def post(self, user_id, workflow_id, next_node):
        utility_functions.select_next_node(
            workflow_id,
            next_node
        )
        return utility_functions.get_workflow_json(workflow_id)


class TaskList(Resource):
    def get(self, user_id):
        workflows = utility_functions.get_workflows_name_and_id_for_tasks(int(user_id))
        return {
            'workflows': workflows
        }

    def post(self, user_id):
        pass


class TaskItem(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument("response", type=str)

    def get(self, user_id, workflow_id):
        return utility_functions.get_tasks_for_user_workflow_json(user_id, workflow_id)

    def post(self, user_id, workflow_id, task_id):
        response = self.parser.parse_args().get('response')
        utility_functions.advance_task(task_id, response)
        return utility_functions.get_tasks_for_user_workflow_json(user_id, workflow_id)


class ResetDB(Resource):
    def get(self):
        utility_functions.reset_db()


@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers',
                         'Content-Type,Authorization')
    response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
    return response


api.add_resource(WorkflowList, "/users/<user_id>/workflows")
api.add_resource(WorkflowItem, "/users/<user_id>/workflows/<workflow_id>")
api.add_resource(WorkflowStart, "/users/<user_id>/workflows/<workflow_id>/start")
api.add_resource(WorkflowInterrupt, "/users/<user_id>/workflows/<workflow_id>/interrupt")
api.add_resource(TaskList, "/users/<user_id>/tasks")
api.add_resource(TaskItem, "/users/<user_id>/tasks/<workflow_id>", "/users/<user_id>/tasks/<workflow_id>/<task_id>")
api.add_resource(ResetDB, "/resetDB")
api.add_resource(SelectAction, "/users/<user_id>/next-action/<workflow_id>/<next_node>")
app.run(debug=True)
