class WorkflowArch:
    def __init__(self,
                 from_node=None,
                 to_node=None,
                 cost=1,
                 resource_type=None):
        self._from = from_node
        self._to = to_node
        self._cost = cost
        self._type = resource_type

    def get_from(self):
        return self._from

    def set_from(self, from_node):
        self._from = from_node

    def get_to(self):
        return self._to

    def set_to(self, to_node):
        self._to = to_node

    def get_cost(self):
        return self._cost

    def set_cost(self, cost):
        self._cost = cost

    def get_resource_type(self):
        return self._type

    def set_resource_type(self, resource):
        self._type = resource

    def has_component(self, component):
        if self._from == component or self._to == component:
            return True
        return False

    def get_dict(self):
        data = {}
        data['to'] = self._to
        data['from'] = self._from
        data['cost'] = self._cost
        data['type'] = self._type
        return data

    def __str__(self):
        return str(self._from) + " -> " + str(self._to) + " " + str(self._cost)


class WorkflowData:
    def __init__(
            self,
            flow_input=None,
            flow_output=None,
            locations=None,
            transitions=None,
            arches=None
    ):
        if locations is None:
            self._locations = []
        else:
            self._locations = locations
        if transitions is None:
            self._transitions = []
        else:
            self._transitions = transitions
        if arches is None:
            self._arches = []
        else:
            self._arches = arches
        self._input = flow_input
        self._output = flow_output

    def get_input(self):
        return self._input

    def set_input(self, flow_input):
        self._input = flow_input
        self._locations.append(flow_input)

    def get_output(self):
        return self._output

    def set_output(self, flow_output):
        self._output = flow_output
        self._locations.append(flow_output)

    def get_locations(self):
        return self._locations

    def add_location(self, location):
        self._locations.append(location)

    def set_locations(self, locations):
        self._locations = locations

    def get_transitions(self):
        return self._transitions

    def add_transition(self, transition):
        self._transitions.append(transition)

    def set_transitions(self, transitions):
        self._transitions = transitions

    def get_arches(self):
        return self._arches

    def add_arch(self, workflow_arch: WorkflowArch):
        self._arches.append(workflow_arch)

    def set_arches(self, arches):
        self._arches = arches

    def get_arches_with_component(self, component):
        return self.get_arches_to_component(component) + self.get_arches_from_component(component)

    def get_arches_to_component(self, component):
        arches_to_component = []
        for arch in self._arches:
            if arch.get_to() == component:
                arches_to_component.append(arch)
        return arches_to_component

    def get_arches_from_component(self, component):
        arches_from_component = []
        for arch in self._arches:
            if arch.get_from() == component:
                arches_from_component.append(arch)
        return arches_from_component

    def get_dict(self):
        data = {}
        data['locations'] = self._locations
        data['transitions'] = self._transitions
        data['arches'] = []
        for arch in self._arches:
            data['arches'].append(arch.get_dict())
        data['input'] = self._input
        data['output'] = self._output
        return data

    def set_dict(self, data):
        self._input = data['input']
        self._output = data['output']
        self._transitions = data['transitions']
        self._locations = data['locations']
        for arch in data['arches']:
            self._arches.append(WorkflowArch(
                arch['from'],
                arch['to'],
                arch['cost'],
                arch.get('type')
            ))

    def __str__(self):
        rez = "locations: " + str(self._locations)
        rez += "\ntransitions: " + str(self._transitions) + "\narches:\n"
        for arch in self._arches:
            rez += str(arch) + "\n"
        return rez
