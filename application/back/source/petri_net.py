from petri_marking import PetriMarking, IllegalMarkingException
from workflow_data import WorkflowData


class PetriNode:
    def __init__(self, petri_marking: PetriMarking):
        self._petri_marking = petri_marking

    def get_marking(self):
        return self._petri_marking

    def set_marking(self, petri_marking: PetriMarking):
        self._petri_marking = petri_marking

    def __eq__(self, other):
        if type(other) is type(self):
            return self._petri_marking == other._petri_marking
        return NotImplemented

    def __str__(self):
        return str(self._petri_marking)

    def __hash__(self):
        return hash(self._petri_marking)


class PetriNet:
    def __init__(self, workflow_data: WorkflowData):
        self._graph = dict()
        self._graphT = dict()
        input_marking = PetriMarking(workflow_data.get_locations())
        self._workflow_data = workflow_data
        input_marking.increment_value_at_location(workflow_data.get_input(), 1)
        self._current_node = PetriNode(input_marking)
        self._create_nodes(self._current_node)

    def _create_nodes(self, petri_node: PetriNode):
        if petri_node not in self._graph:
            self._graph[petri_node] = dict()
        if petri_node not in self._graphT:
            self._graphT[petri_node] = dict()

        for transition in self._workflow_data.get_transitions():
            try:
                current_marking = PetriMarking.generate_marking_from(
                    petri_node.get_marking(),
                    self._workflow_data,
                    transition)

                for node in self._graph.keys():
                    if current_marking > node.get_marking():
                        print(current_marking)
                        print(node.get_marking())
                        raise InfinitePetriNetException("The Workflow cannot "
                                                        "finish, there will always "
                                                        "be a possible action")

                neighbour = PetriNode(current_marking)
                self._graph[petri_node][transition] = neighbour
                if neighbour not in self._graphT:
                    self._graphT[neighbour] = dict()
                self._graphT[neighbour][transition] = petri_node
            except IllegalMarkingException:
                pass

        for transition in self._graph[petri_node].keys():
            if self._graph[petri_node][transition] not in self._graph:
                self._create_nodes(self._graph[petri_node][transition])

    def is_current_marking_out(self):
        out_marking = PetriMarking(self._workflow_data.get_locations())
        out_marking.increment_value_at_location(self._workflow_data.get_output())
        out_node = PetriNode(out_marking)
        return self._current_node == out_node

    def add_node(self,
                 petri_node: PetriNode,
                 neighbours: "Dict of transition -> PetriNode" = None):
        if neighbours in None:
            self._graph[petri_node] = dict()
        else:
            self._graph[petri_node] = neighbours

    def set_current_marking(self, marking: PetriNode):
        self._current_node = marking

    def get_current_marking(self):
        return self._current_node

    def get_current_possible_tansitions(self):
        return list(self._graph[self._current_node].keys())

    def get_neighbours(self, petri_node: PetriNode=None):
        if petri_node:
            return self._graph[petri_node]
        return self._graph[self._current_node]

    def advance(self, transition):
        self._current_node = self._graph[self._current_node][transition]

    def soundness(self):
        pseudo_viable, impossible_transitions = self._is_pseudo_viable()
        if not pseudo_viable:
            msg = str(impossible_transitions[0])
            for tran in impossible_transitions[1:]:
                msg += " " + str(tran)
            raise NotPseudoViable(msg)

        if not self._is_O_home_marking():
            raise OIsNotHomeMarking("the workflow cannot finish correctly")

    def get_workflow_data(self):
        return self._workflow_data

    def _is_pseudo_viable(self):
        impossible_transitions = []
        for transition in self._workflow_data.get_transitions():
            isIn = False
            for node in self._graph:
                if transition in self._graph[node].keys():
                    isIn = True
                    break
            if not isIn:
                impossible_transitions.append(transition)
        if len(impossible_transitions) != 0:
            return False, impossible_transitions
        return True, impossible_transitions

    def _is_O_home_marking(self):
        SCC = self._strongly_connected_components()
        interiorSCC = self._interior_strongly_connected_components(SCC)

        if len(list(interiorSCC.keys())) != 1:
            return False

        outMarking = PetriMarking(self._workflow_data.get_locations())
        outMarking.change_location_value(self._workflow_data.get_output(), 1)
        outMarking = PetriNode(outMarking)

        if outMarking not in list(interiorSCC.values())[0]:
            return False
        return True

    def _strongly_connected_components(self):
        self._used = dict.fromkeys(self._graph.keys(), 0)
        self._where = dict.fromkeys(self._graph.keys(), -1)
        self._discoverd = []
        SCC = dict()

        for node in self._graph.keys():
            if self._used[node] == 0:
                self._DFP(node)

        count = 0
        for node in reversed(self._discoverd):
            if self._where[node] == -1:
                self._DFM(node, count)
                count += 1

        for node in self._graph.keys():
            if self._where[node] not in SCC.keys():
                SCC[self._where[node]] = []
            SCC[self._where[node]] += [node]

        return SCC

    def _DFP(self, node):
        self._used[node] = 1
        for neighbour in self._graph[node].values():
            if self._used[neighbour] == 0:
                self._DFP(neighbour)
        self._discoverd.append(node)

    def _DFM(self, node, count):
        self._where[node] = count
        for neighbour in self._graphT[node].values():
            if self._where[neighbour] == -1:
                self._DFM(neighbour, count)

    def _interior_strongly_connected_components(self, SCC):
        interiorSCC = dict()

        for sccNumber in SCC:
            all = True
            for node in SCC[sccNumber]:
                if not set(self._graph[node].values()) <= set(SCC[sccNumber]):
                    all = False
                    break
            if all:
                interiorSCC[sccNumber] = SCC[sccNumber]

        return interiorSCC

    def __str__(self):
        return self._graph_string(self._graph)

    def _graph_string(self, graph):
        graph_string = ""
        for petri_node in graph:
            graph_string += str(petri_node) + ":"
            for node in graph[petri_node].keys():
                graph_string += " (" + str(node) + ": "
                graph_string += str(graph[petri_node][node]) + "),"
            graph_string += "\n"
        return graph_string


class InfinitePetriNetException(Exception):
    pass


class OIsNotHomeMarking(Exception):
    pass


class NotPseudoViable(Exception):
    pass
