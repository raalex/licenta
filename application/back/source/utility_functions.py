import db_model
import hashlib
import random
import workflow_data
import petri_net
import convert_workflow
import datetime
import copy
import json


blank_workflow = {
    "edges": [],
    "nodes": [{
        "id": "in",
        "label": "in",
        "shape": "ellipse",
        "x": 10,
        "y": 0,
        "color": "#90CAF9",
        "fixed": {
            "x": False,
            "y": False
        }
    }, {
        "id": "out",
        "label": "out",
        "shape": "ellipse",
        "x": -10,
        "y": 0,
        "color": "#90CAF9",
        "fixed": {
            "x": False,
            "y": False
        }
    }]}
blank_petri_representation = {
    "arches": [],
    "locations": [
        "in", "out"
    ],
    "transitions": [],
    "input": "in",
    "output": "out"
}
action_state_color = {
    "to_do": "#FFCC80",
    "doing": "#CE93D8 ",
    "done": "#A5D6A7",
    "pending": "#EEEEEE",
    "must_choose": "#FFF59D"
}


def get_new_workflow_id(user_id):
    return len(get_users_workflows(user_id))


def get_workflows_name_and_id(user_id):
    return [{
        'id': w.id,
        'name': w.name
    } for w in get_users_workflows(user_id)]


def get_workflows_name_and_id_for_tasks(user_id):
    return [{
        'id': w.id,
        'name': w.name
    } for w in get_users_tasks_workflows(user_id)]


def get_workflow_name(workflow_id):
    return get_workflow(workflow_id).name


def get_workflow(workflow_id):
    return db_model.Workflow.query.get(workflow_id)


def get_workflow_json(workflow_id):
    workflow = get_workflow(workflow_id)
    workflow_json = {
        'id': workflow.id,
        'name': workflow.name,
        'workflow': workflow.original,
        'user_type': get_user_types(),
        'state': workflow.state
    }
    return workflow_json


def get_task_json(task=None, task_id=None):
    task_json = None
    if task_id is not None:
        task = db_model.Task.query.get(task_id)
    if task is not None:
        task_json = {
            'id': task.id,
            'title': task.title,
            'description': task.description,
            'inResources': task.in_resources,
            'outResources': task.out_resources,
            'state': task.state.value,
            'responseValues': task.response_values['val']
        }
    return task_json


def get_tasks_for_user_workflow_json(user_id, workflow_id):
    # tasks = db_model.db.session.query(db_model.Task).filter_by(workflow_id=workflow_id, employee_id=user_id).all()
    tasks = db_model.Task.query.filter_by(workflow_id=workflow_id, employee_id=user_id).all()
    tasks_to_do_json = []
    tasks_doing_json = []
    tasks_done_json = []
    for task in tasks:
        if task.state == db_model.StateTypes.to_do:
            tasks_to_do_json.append(get_task_json(task))
        elif task.state == db_model.StateTypes.doing:
            tasks_doing_json.append(get_task_json(task))
        else:
            tasks_done_json.append(get_task_json(task))
    return {
        'to_do': tasks_to_do_json,
        'doing': tasks_doing_json,
        'done': tasks_done_json
    }


def create_new_workflow(user_id, workflow_name):
    workflow = db_model.Workflow(
        user_id,
        workflow_name,
        blank_workflow,
        blank_petri_representation
    )
    db_model.db.session.add(workflow)
    db_model.db.session.commit()
    return workflow.id


def update_workflow(workflow_id, workflow_name, original_workflow):
    workflow = db_model.Workflow.query.get(workflow_id)
    workflow.name = workflow_name
    workflow.original = original_workflow
    db_model.db.session.commit()
    petri_workflow, or_split_data = verify_workflow(original_workflow)
    workflow.petri_representation = petri_workflow
    workflow.or_split_data = or_split_data
    db_model.db.session.commit()


def create_new_user(username, name, password, job, emp_date):
    emp = db_model.Employee(username, name, password, job, emp_date)
    db_model.db.session.add(emp)
    db_model.db.session.commit()
    return emp.id


def get_user(user_id=None, user_name=None):
    if user_id is not None:
        return db_model.Employee.query.get(user_id)
    elif user_name is not None:
        return db_model.Employee.query.filter_by(username=user_name).first()
    return None


def get_users_tasks_workflows(user_id):
    workflows_id = set()
    workflows = []
    tasks = db_model.Task.query.filter_by(employee_id=user_id).all()
    for task in tasks:
        workflows_id.add(task.workflow_id)
    for id in workflows_id:
        workflows.append(db_model.Workflow.query.get(id))
    return list(workflows)


def get_users_workflows(user_id):
    return db_model.Workflow.query.filter_by(employee_id=user_id).all()


def get_users_tasks(user_id):
    return db_model.Employee.query.get(user_id).workflows


def verify_password(user_id, password):
    user = get_user(user_id)
    return check_password(password, user.password, user.salt)


def get_hashed_password(plain_password):
    salt = str(random.getrandbits(512))
    pass_salt = str(plain_password + salt).encode('utf-8')
    return hashlib.sha512(pass_salt).hexdigest(), salt


def check_password(plain_password, hash_password, salt):
    pass_salt = str(plain_password + salt).encode('utf-8')
    return hashlib.sha512(pass_salt).hexdigest() is hash_password


def verify_workflow(workflow):
    data = workflow_data.WorkflowData()
    or_split_data = {}

    json_petri = convert_workflow.convert_to_petri_representation(workflow)
    for loc in json_petri['nodes']:
        if loc['id'] == "in":
            data.set_input(loc['id'])
        elif loc['id'] == "out":
            data.set_output(loc['id'])
        else:
            data.add_location(loc['id'])
        aux = loc.get('switchValues')
        if aux:
            or_split_data[loc['id']] = aux
    for tran in json_petri['transitions']:
        data.add_transition(tran['id'])
    for arch in json_petri['edges']:
        if (arch['from'] in data.get_locations() and arch['to'] in data.get_locations()) or (arch['from'] in data.get_transitions() and arch['to'] in data.get_transitions()):
            raise MallforemedNet("There is an edge between in and out node or action problem")
        data.add_arch(workflow_data.WorkflowArch(arch['from'], arch['to'], resource_type=arch.get('label')))
    net = petri_net.PetriNet(data)
    net.soundness()
    return data.get_dict(), or_split_data


def reset_db():
    db_model.db.drop_all()
    db_model.db.create_all()

    emp1 = db_model.Employee(
        "rares.stan",
        "Rares Stan",
        "123456",
        db_model.JobType.developer,
        datetime.datetime.utcnow()
    )
    emp2 = db_model.Employee(
        "b.a",
        "B A",
        "123456",
        db_model.JobType.manager,
        datetime.datetime.utcnow()
    )

    emp3 = db_model.Employee(
        "tucar.liana",
        "Liana",
        "123456",
        db_model.JobType.developer,
        datetime.datetime(2016, 5, 12, 12)
    )

    emp4 = db_model.Employee(
        "dan.marculet",
        "Dan Marculet",
        "123456",
        db_model.JobType.developer,
        datetime.datetime.utcnow()
    )

    db_model.db.session.add(emp1)
    db_model.db.session.add(emp2)
    db_model.db.session.add(emp3)
    db_model.db.session.add(emp4)
    db_model.db.session.commit()

    original_str0 = (
        '{"nodes":[{"userState":[],"outResources":["cerere transfer"],"color":"#EEEEEE","employees":["rares.stan",'
        '"dan.marculet"],"inResources":[],"user_type":"developer","label":"Intocmit cerere","shape":"box","allow_se'
        'lection":true,"y":-60.251557265494135,"x":-146.10438872422006,"fixed":{"y":false,"x":false},"user_number":2'
        ',"id":"1","description":"Intocmit cererea de trtansfer dupa modelul standard"},{"userState":[],"outResource'
        's":[],"color":"#EEEEEE","employees":["rares.stan","dan.marculet"],"inResources":[],"user_type":"developer",'
        '"label":"Depus cerere","shape":"box","allow_selection":true,"y":-212.4542373324958,"x":-116.55916259356681,"'
        'fixed":{"y":false,"x":false},"user_number":2,"id":"2"},{"userState":[],"outResources":[],"color":"#EEEEEE","'
        'employees":["rares.stan","dan.marculet"],"inResources":[],"user_type":"developer","label":"Plata taxa","sha'
        'pe":"box","allow_selection":true,"y":-124.71386882328308,"x":60.71219419035278,"fixed":{"y":false,"x":false}'
        ',"user_number":2,"id":"3"},{"userState":[],"outResources":[],"color":"#EEEEEE","employees":[],"inResources"'
        ':[],"user_type":"manager","label":"Verificat cerere","shape":"box","allow_selection":true,"y":-281.393098304'
        '0201,"x":-87.01393646291353,"fixed":{"y":false,"x":false},"user_number":1,"id":"4"},{"userState":[],"outReso'
        'urces":[],"color":"#EEEEEE","employees":[],"inResources":[],"user_type":"manager","label":"Cerere invalida","'
        '":"box","allow_selection":true,"y":-259.01035123534336,"x":237.0882410915253,"fixed":{"y":false,"x":false},"'
        'user_number":1,"id":"5"},{"userState":[],"outResources":[],"color":"#EEEEEE","employees":[],"inResources":[]'
        ',"user_type":"manager","label":"Cerere valida","shape":"box","allow_selection":true,"y":-158.73564436767168,'
        '"x":254.0991288637196,"fixed":{"y":false,"x":false},"user_number":1,"id":"6"},{"color":"#90CAF9","label":"in'
        '","shape":"ellipse","y":0,"x":10,"fixed":{"y":false,"x":false},"id":"in"},{"color":"#90CAF9","label":"out","'
        'shape":"ellipse","y":0,"x":-10,"fixed":{"y":false,"x":false},"id":"out"},{"color":"#90CAF9","label":"And Spli'
        't","shape":"ellipse","allow_selection":false,"y":-112.17953046482411,"x":-100.44358470411957,"fixed":{"y":fal'
        'se,"x":false},"id":"298485a5-56cf-4275-a48f-f100d69e9792"},{"color":"#90CAF9","label":"Or Split","shape":"ell'
        'ipse","allow_selection":false,"y":-223.19795592546063,"x":135.02291445835948,"fixed":{"y":false,"x":false},"i'
        'd":"ac2616e9-5d96-4e3e-be43-a4e20c9dbb9f"},{"color":"#90CAF9","label":"Or Join","shape":"ellipse","allow_selec'
        'tion":false,"y":-194.54803967755444,"x":273.79594628415515,"fixed":{"y":false,"x":false},"id":"f94703bd-8761-'
        '403d-be82-126ebfef7d6a"},{"color":"#90CAF9","label":"And Join","shape":"ellipse","allow_selection":false,"y"'
        ':58.82465713986599,"x":292.59745382184354,"fixed":{"y":false,"x":false},"id":"2bb04dd5-9afe-49ee-ba21-191dd7'
        '0be5e8"}],"edges":[{"color":"grey","to":"1","from":"in","id":"95583c2a-bdaf-4bb5-b5be-83b81db2b962"},{"colo'
        'r":"grey","to":"298485a5-56cf-4275-a48f-f100d69e9792","from":"1","id":"8ace3b86-337d-419d-be26-d04be1d6627b"}'
        ',{"color":"grey","to":"2","from":"298485a5-56cf-4275-a48f-f100d69e9792","id":"88757eb4-a32c-41a2-b805-7e0530'
        'e4e983"},{"color":"grey","to":"3","from":"298485a5-56cf-4275-a48f-f100d69e9792","id":"a587d74e-c696-4617-b6'
        '1a-d05adc4ac8c8"},{"color":"grey","to":"4","from":"2","id":"f04f5086-b37f-45d2-8839-d673527ac062"},{"color":"'
        'grey","to":"ac2616e9-5d96-4e3e-be43-a4e20c9dbb9f","from":"4","id":"87b6110a-9bd6-4600-a0ce-a67d631cddb5"},{"'
        'color":"grey","to":"5","from":"ac2616e9-5d96-4e3e-be43-a4e20c9dbb9f","id":"0af5a19a-2d57-420b-9ab7-e78ef7d9'
        '8282"},{"color":"grey","to":"6","from":"ac2616e9-5d96-4e3e-be43-a4e20c9dbb9f","id":"910ed3ad-fe46-48b7-a783-'
        '30ba599b6bb9"},{"color":"grey","to":"f94703bd-8761-403d-be82-126ebfef7d6a","from":"5","id":"acfb2eb8-8c94-'
        '4193-8b76-268e4455257b"},{"color":"grey","to":"f94703bd-8761-403d-be82-126ebfef7d6a","from":"6","id":"e014'
        '1737-cce8-4e58-96e6-ef2289ccb123"},{"color":"grey","to":"2bb04dd5-9afe-49ee-ba21-191dd70be5e8","from":"f94'
        '703bd-8761-403d-be82-126ebfef7d6a","id":"9bcff977-4df5-4ff0-8180-14209661f1c2"},{"color":"grey","to"'
        ':"2bb04dd5-9afe-49ee-ba21-191dd70be5e8","from":"3","id":"de995fdb-fa52-44cb-9961-ec0a68916'
        '346"},{"color":"grey","to":"out","from":"2bb04dd5-9afe-49ee-ba21-191dd70be5e8","id":"1438ea66-3466-4437-'
        'a88f-00610e8ecb73"}]}'
                     )

    original_str1 = (
        '{"nodes":[{"fixed":{"y":false,"x":false},"description":"strjhytjshttyjsht","color":"grey",'
        '"employees":["rares.stan"],"user_type":"developer","response":"works?","shape":"box","allow_'
        'selection":true,"y":-36.296875,"x":-136.96875,"label":"Node 1","user_number":2,"id":"1",'
        '"resources":[]},{"color":"grey","employees":["rares.stan"],"user_type":"developer","label":'
        '"Node 2","shape":"box","allow_selection":true,"y":-122.296875,"x":32.03125,"fixed":{"y":false,'
        '"x":false},"user_number":1,"id":"2","resources":[]},{"color":"grey","employees":[],"user_type":'
        '"manager","label":"Node 3","shape":"box","allow_selection":true,"y":-57.296875,"x":50.03125,'
        '"fixed":{"y":false,"x":false},"user_number":1,"id":"3","resources":[]},{"color":"blue","label":'
        '"in","shape":"ellipse","y":0,"x":10,"fixed":{"y":false,"x":false},"id":"in"},{"color":"blue",'
        '"label":"out","shape":"ellipse","y":0,"x":-10,"fixed":{"y":false,"x":false},"id":"out"},{"color"'
        ':"blue","label":"Or Split","shape":"ellipse","allow_selection":true,"y":-51.296875,"x":0.03125,'
        '"switchValues":[{"case":"true","to":"2","$$hashKey":"object:81","label":"Node 2"},{"case":'
        '"false","to":"3","$$hashKey":"object:82","label":"Node 3"}],"fixed":{"y":false,"x":false},"id":'
        '"12b1821d-96e2-426a-9983-2faf2c9e09e1"},{"color":"blue","label":"Or Join","shape":"ellipse",'
        '"allow_selection":false,"y":-60.296875,"x":108.03125,"fixed":{"y":false,"x":false},"id":'
        '"07de454c-bab6-456e-b4db-b39c77c4dc79"}],"edges":[{"color":"grey","to":"1","from":"in","id":'
        '"00e92398-9053-4140-9a35-1e328b48f071"},{"color":"grey","to":'
        '"12b1821d-96e2-426a-9983-2faf2c9e09e1","from":"1","id":"001ec526-a406-4a0f-bb57-e2b5d82d796c"},'
        '{"color":"grey","to":"2","from":"12b1821d-96e2-426a-9983-2faf2c9e09e1","id":'
        '"8372a9f8-2497-4d22-9205-74ec34983ed5"},{"color":"grey","to":"3","from":'
        '"12b1821d-96e2-426a-9983-2faf2c9e09e1","id":"31b50267-7f1b-4ea3-a80e-f5058c7d0334"},'
        '{"color":"grey","to":"07de454c-bab6-456e-b4db-b39c77c4dc79","from":"2","id":'
        '"881545ac-80f3-4b88-8778-534301a21c8c"},{"color":"grey","to":'
        '"07de454c-bab6-456e-b4db-b39c77c4dc79","from":"3","id":"40c493c1-1deb-4445-9eea-eee7a9bfc8d1"},'
        '{"color":"grey","to":"out","from":"07de454c-bab6-456e-b4db-b39c77c4dc79",'
        '"id":"aea7e239-e03f-45c9-919c-600772420ab2"}]}'
                     )

    original_str2 = (
        '{"nodes":[{"color":"grey","employees":["rares.stan"],"user_type":"developer","label":"Node 1","shape":"box",'
        '"allow_selection":true,"y":-196.296875,"x":-12.641357421875,"fixed":{"y":false,"x":false},"user_number":1,'
        '"id":"1","resources":[]},{"color":"grey","employees":["rares.stan"],"user_type":"developer","label":"Node 2",'
        '"shape":"box","allow_selection":true,"y":-87.296875,"x":23.358642578125,"fixed":{"y":false,"x":false},'
        '"user_number":1,"id":"2","resources":[]},{"color":"blue","label":"in","shape":"ellipse","y":0,"x":10,"fixed":'
        '{"y":false,"x":false},"id":"in"},{"color":"blue","label":"out","shape":"ellipse","y":0,"x":-10,"fixed":{"y"'
        ':false,"x":false},"id":"out"},{"color":"blue","label":"And Split","shape":"ellipse","allow_selection":false,'
        '"y":-102.296875,"x":-103.641357421875,"fixed":{"y":false,"x":false},"id":'
        '"e8c2a2a9-17fa-452d-82ee-526b111b0c62"},{"color":"blue","label":"And Join","shape":"ellipse",'
        '"allow_selection":false,"y":-131.296875,"x":127.358642578125,"fixed":{"y":false,"x":false},"id":'
        '"ea790827-d1af-43c7-913b-069c3b97f35f"}],"edges":[{"color":"grey","to":"e8c2a2a9-17fa-452d-82ee-526b111b0c62",'
        '"from":"in","id":"a680185e-9ba3-4170-8c88-cb76845150d1"},{"color":"grey","to":"1","from":'
        '"e8c2a2a9-17fa-452d-82ee-526b111b0c62","id":"bfa5ae49-7cc9-4cb1-949c-acc168110fd2"},{"color":"grey","to":"2",'
        '"from":"e8c2a2a9-17fa-452d-82ee-526b111b0c62","id":"ddb88d8a-5d13-46e9-ba5d-3559130e8a3a"},{"color":"grey",'
        '"to":"ea790827-d1af-43c7-913b-069c3b97f35f","from":"1","id":"86f3dd90-207f-4373-91a2-2347c1bebeae"},'
        '{"color":"grey","to":"ea790827-d1af-43c7-913b-069c3b97f35f","from":"2","id":'
        '"7bb88e04-eb5a-437b-af52-8d5fba1f0799"},{"color":"grey","to":"out","from":'
        '"ea790827-d1af-43c7-913b-069c3b97f35f","id":"9042bc32-937a-4530-a758-2c54580561c8"}]}'
    )

    original0 = json.loads(original_str0)
    original1 = json.loads(original_str1)
    original2 = json.loads(original_str2)
    workflow0 = db_model.Workflow(
        emp1.id,
        "Cerere Transfer",
        blank_workflow,
        blank_petri_representation
    )
    workflow1 = db_model.Workflow(
        emp1.id,
        "New Network 1",
        blank_workflow,
        blank_petri_representation
    )

    workflow2 = db_model.Workflow(
        emp1.id,
        "New Network 2",
        blank_workflow,
        blank_petri_representation
    )
    db_model.db.session.add(workflow0)
    # db_model.db.session.add(workflow1)
    # db_model.db.session.add(workflow2)
    db_model.db.session.commit()

    update_workflow(workflow0.id, workflow0.name, original0)
    # update_workflow(workflow1.id, workflow1.name, original1)
    # update_workflow(workflow2.id, workflow2.name, original2)


def get_aux_transitions(transitions):
    aux = []
    for transition in transitions:
        if isinstance(transition, str):
            if transition[:14] == "aux_transition":
                aux.append(transition)
    return aux


def get_transition_user_type(workflow_data_dict, transition_id):
    for node in workflow_data_dict['nodes']:
        if node['id'] == transition_id:
            return node['user_type']
    return None


def find_original_transition(workflow, transition_id):
    for node in workflow.original['nodes']:
        if node['id'] == transition_id:
            return node
    return None


def all_action_tasks_done(action_id, workflow_id):
    tasks = db_model.Task.query.filter_by(action_id=action_id, workflow_id=workflow_id).all()
    for task in tasks:
        if task.state != db_model.StateTypes.done:
            return False
    return True


def advance_task(task_id, response):
    task = db_model.Task.query.get(task_id)
    workflow = get_workflow(task.workflow_id)
    user = db_model.Employee.query.get(task.employee_id)

    if task.state == db_model.StateTypes.to_do:
        task.state = db_model.StateTypes.doing
        nodes = copy.deepcopy(workflow.original['nodes'])
        for node in nodes:
            if node['id'] == task.action_id:
                node['color'] = action_state_color['doing']
                for u in node['userState']:
                    if u['username'] == user.username:
                        u['state'] = 'Doing'
                        break
                break
        workflow.original['nodes'] = nodes
        db_model.db.session.commit()
    elif task.state == db_model.StateTypes.doing:
        task.state = db_model.StateTypes.done
        task.response = response
        nodes = copy.deepcopy(workflow.original['nodes'])
        for node in nodes:
            if node['id'] == task.action_id:
                for u in node['userState']:
                    if u['username'] == user.username:
                        u['state'] = 'Done'
                        break
                break
        workflow.original['nodes'] = nodes
        db_model.db.session.commit()

        if all_action_tasks_done(task.action_id, task.workflow_id):
            for node in nodes:
                if node['id'] == task.action_id:
                    node['color'] = action_state_color['done']
                    break
            workflow.original['nodes'] = nodes
            db_model.db.session.commit()
            workflow_action_done(workflow, task.action_id, task.response_values['val'])


def find_users_for_task(user_type, number, preferred_users):
    all_user_type = [[user, 1000] for user in db_model.Employee.query.filter_by(job=user_type).all()]
    today = datetime.datetime.utcnow()
    for user in all_user_type:
        total_tasks = len(db_model.Task.query.filter(
            db_model.Task.employee_id == user[0].id,
            db_model.Task.state != db_model.StateTypes.done
        ).all())
        user[1] -= 100 * total_tasks
        user[1] += 10 * (
            (today.year - user[0].employment_data.year) * 12 +
            today.month - user[0].employment_data.month
        )
        if user[0].username in preferred_users:
            user[1] += 300
    all_user_type.sort(key=lambda u: u[1], reverse=True)
    all_user_type = list(zip(*all_user_type))[0]
    if number < len(all_user_type):
        return all_user_type[:number]
    return all_user_type


def get_next_locations_switch_values(workflow, net, transition):
    arches = net.get_workflow_data().get_arches_with_component(transition)
    for arch in arches:
        aux = workflow.or_split_data.get(arch.get_to())
        if aux:
            return aux


def get_transition_for_or_split(workflow_id, net, response_values, action_id, user_response):
    tasks = db_model.Task.query.filter_by(workflow_id=workflow_id, action_id=action_id).all()
    if not user_response:
        all_same = True
        for task in tasks:
            if task.response != tasks[0].response:
                all_same = False
                break
        if not all_same:
            raise CannotDecideActionException()
        response = tasks[0].response
    else:
        response = user_response
    next_node = None
    for response_value in response_values:
        if response_value['case'] == response:
            next_node = response_value['to']
    node = None
    arches = net.get_workflow_data().get_arches_with_component(action_id)
    for arch in arches:
        if arch.get_from() == action_id:
            node = arch.get_to()
            break
    arches = net.get_workflow_data().get_arches_with_component(node)
    for arch in arches:
        if arch.get_resource_type() == next_node:
            return arch.get_to()
    raise CannotDecideActionException()


def select_next_node(workflow_id, next_node_id):
    workflow = get_workflow(workflow_id)
    data = workflow_data.WorkflowData()
    data.set_dict(workflow.petri_representation)
    net = petri_net.PetriNet(data)
    net.set_current_marking(petri_net.PetriNode(petri_net.PetriMarking(markings=workflow.current_marking)))
    neighbours = net.get_neighbours()
    transition_list = list(neighbours.keys())
    aux_transitions = get_aux_transitions(transition_list)
    if next_node_id not in transition_list and len(aux_transitions) != 0:
        for transition in aux_transitions:
            edges = data.get_arches_to_component(transition)
            if edges[0].get_resource_type() == next_node_id:
                net.advance(transition)
                neighbours = net.get_neighbours()
                transition_list = list(neighbours.keys())
                break
    nodes = workflow.original['nodes']
    for node in nodes:
        if node['color'] == action_state_color['must_choose'] and node['id'] not in transition_list:
            node['color'] = action_state_color['pending']
    workflow.original['nodes'] = nodes
    db_model.db.session.commit()
    if next_node_id in transition_list:
        # net.advance(next_node_id)
        get_tasks(workflow, net, next_node_id)
    """
    net.advance(list(neighbours.keys())[0])
    neighbours = net.get_neighbours()
    if len(neighbours) > 1:
        for t in neighbours:
            arches = data.get_arches_to_component(t)
            for arch in arches:
                if arch.get_resource_type() == next_node_id:
                    net.advance(t)
                    get_tasks(workflow, net)
                    return"""


def is_or_split_transition(transition, workflowdata, or_split_next):
    edges = workflowdata.get_arches_to_component(transition)
    if edges[0].get_resource_type():
        or_split_next.append(edges[0].get_resource_type())
        return True
    return False


def advance_workflow(workflow, net, action_id=None):
    if action_id:
        net.advance(action_id)
    workflowdata = net.get_workflow_data()
    while True:
        # cand dau de un or node tre sa ma opresc, ca sa astept sa aleaga utilizatorul
        or_split_next = []
        possible_transitions = net.get_current_possible_tansitions()
        aux_transitions = get_aux_transitions(possible_transitions)
        aux_transitions = [x for x in aux_transitions if not is_or_split_transition(x, workflowdata, or_split_next)]
        if len(aux_transitions) == 0:
            break
        net.advance(aux_transitions[0])
    workflow.current_marking = net.get_current_marking().get_marking().get_markings()
    if net.is_current_marking_out():
        workflow.state = False
        db_model.db.session.commit()
        return
    nodes = copy.deepcopy(workflow.original['nodes'])
    for node in nodes:
        if node['id'] in possible_transitions or node['id'] in or_split_next:
            if node['color'] == action_state_color['pending']:
                node['color'] = action_state_color['must_choose']
    workflow.original['nodes'] = nodes
    db_model.db.session.commit()
    """
    if response_values:
        try:
            transition = get_transition_for_or_split(workflow.id, net, response_values, action_id, user_response)
            net.advance(transition)
        except CannotDecideActionException:
            nodes = copy.deepcopy(workflow.original['nodes'])
            node_id = None
            for edge in workflow.original['edges']:
                if edge['from'] == action_id:
                    node_id = edge['to']
                    break
            for node in nodes:
                if node['id'] == node_id:
                    node['color'] = action_state_color['must_choose']
                    break
            workflow.original['nodes'] = nodes
            db_model.db.session.commit()
            raise CannotDecideActionException
    get_tasks(workflow, net)"""


def get_tasks(workflow, net, transition):
    tasks = []
    transition_in_progress = [t.action_id for t in db_model.Task.query.filter_by(workflow_id=workflow.id).all()]
    user_type = get_transition_user_type(workflow.original, transition)
    original_transition = find_original_transition(workflow, transition)
    selected_users = find_users_for_task(
        user_type,
        original_transition.get('user_number'),
        original_transition.get('employees')
    )
    switch_values = get_next_locations_switch_values(workflow, net, transition)
    for user in selected_users:
        new_task = {
            'user_id': user.id,
            'workflow_id': workflow.id,
            'title': original_transition.get('label'),
            'description': original_transition.get('description'),
            'inResources': original_transition.get('inResources'),
            'outResources': original_transition.get('outResources'),
            'action_id': original_transition.get('id'),
            'response_values': {'val': switch_values}
        }
        tasks.append(new_task)
    create_tasks(tasks)
    nodes = copy.deepcopy(workflow.original['nodes'])
    for node in nodes:
        if node['id'] == transition and node['id'] not in transition_in_progress:
            node['color'] = action_state_color['to_do']
            node['userState'] = get_user_state_for_action(workflow.id, node['id'])
    workflow.original['nodes'] = nodes
    db_model.db.session.commit()


"""
def get_tasks(workflow, net):
    while True:
        possible_transitions = net.get_current_possible_tansitions()
        aux_transitions = get_aux_transitions(possible_transitions)
        if len(aux_transitions) == 0:
            break
        net.advance(aux_transitions[0])
    workflow.current_marking = net.get_current_marking().get_marking().get_markings()
    if net.is_current_marking_out():
        workflow.state = False
        db_model.db.session.commit()
    else:
        # db_model.db.session.commit()
        tasks = []
        transition_in_progress = [t.action_id for t in db_model.Task.query.filter_by(workflow_id=workflow.id).all()]

        for transition in possible_transitions:
            if transition not in transition_in_progress:
                user_type = get_transition_user_type(workflow.original, transition)
                original_transition = find_original_transition(workflow, transition)
                selected_users = find_users_for_task(
                    user_type,
                    original_transition.get('user_number'),
                    original_transition.get('employees')
                )
                switch_values = get_next_locations_switch_values(workflow, net, transition)
                for user in selected_users:
                    new_task = {
                        'user_id': user.id,
                        'workflow_id': workflow.id,
                        'title': original_transition.get('label'),
                        'description': original_transition.get('description'),
                        'resources': original_transition.get('resources'),
                        'action_id': original_transition.get('id'),
                        'response_values': {'val': switch_values}
                    }
                    tasks.append(new_task)
        create_tasks(tasks)
        nodes = copy.deepcopy(workflow.original['nodes'])
        for node in nodes:
            if node['id'] in possible_transitions and node['id'] not in transition_in_progress:
                node['color'] = action_state_color['to_do']
                node['userState'] = get_user_state_for_action(workflow.id, node['id'])
        workflow.original['nodes'] = nodes
        db_model.db.session.commit()
"""


def verify_workflow_for_starting(workflow):
    predef_nodes = ["in", "out", "And Join", "And Split", "Or Join", "Or Split"]
    for node in workflow.original['nodes']:
        if (not node.get('user_type') or not node.get('user_number')) and node['label'] not in predef_nodes:
            return False
    return True


def start_workflow(workflow_id):
    session = db_model.db.session
    workflow = db_model.Workflow.query.get(workflow_id)
    if not verify_workflow_for_starting(workflow):
        raise Exception("must specify employee role and number for each action")
    old_tasks = db_model.Task.query.filter_by(workflow_id=workflow_id).all()
    predef_nodes = ["in", "out", "And Join", "And Split", "Or Join", "Or Split"]
    for task in old_tasks:
        session.delete(task)
    nodes = copy.deepcopy(workflow.original['nodes'])
    for node in nodes:
        if node['label'] not in predef_nodes:
            node['color'] = action_state_color['pending']
            node['userState'] = []
    workflow.original['nodes'] = nodes
    workflow.state = True
    data = workflow_data.WorkflowData()
    data.set_dict(workflow.petri_representation)
    net = petri_net.PetriNet(data)
    session.commit()
    advance_workflow(workflow, net)


def workflow_action_done(workflow, action_id, response_values, user_response=None):
    # TO-DO: use the response values and user response, maybe store them somewhere
    data = workflow_data.WorkflowData()
    data.set_dict(workflow.petri_representation)
    net = petri_net.PetriNet(data)
    net.set_current_marking(petri_net.PetriNode(petri_net.PetriMarking(markings=workflow.current_marking)))
    advance_workflow(workflow, net, action_id)


def create_tasks(tasks):
    for task_data in tasks:
        new_task = db_model.Task(
            task_data['user_id'],
            task_data['workflow_id'],
            task_data['title'],
            task_data['description'],
            task_data['inResources'],
            task_data['outResources'],
            task_data['action_id'],
            task_data['response_values']
        )
        db_model.db.session.add(new_task)
    db_model.db.session.commit()


def get_user_types():
    users = db_model.Employee.query.all()
    user_types = set()
    for user in users:
        user_types.add(user.job.value)
    return list(user_types)


def delete_workflow(workflow_id):
    tasks = db_model.Task.query.filter_by(workflow_id=workflow_id).all()
    for task in tasks:
        db_model.db.session.delete(task)
    db_model.db.session.delete(get_workflow(workflow_id))
    db_model.db.session.commit()


def get_user_state_for_action(workflow_id, action_id):
    users_tasks = db_model.Employee.query.join(db_model.Task, db_model.Employee.id == db_model.Task.employee_id).\
                                   add_columns(db_model.Employee.username, db_model.Task.state).\
                                   filter(db_model.Task.workflow_id == int(workflow_id)).\
                                   filter(db_model.Task.action_id == str(action_id)).\
                                   all()
    # return user_tasks
    users_state = []
    for u in users_tasks:
        users_state.append({'username': u[1], 'state': "To Do"})
    return users_state


def interrupt_workflow(workflow_id):
    old_tasks = db_model.Task.query.filter_by(workflow_id=workflow_id).all()
    predef_nodes = ["in", "out", "And Join", "And Split", "Or Join", "Or Split"]
    for task in old_tasks:
        db_model.db.session.delete(task)
    workflow = db_model.Workflow.query.get(workflow_id)
    nodes = copy.deepcopy(workflow.original['nodes'])
    for node in nodes:
        if node['label'] not in predef_nodes:
            node['color'] = action_state_color['pending']
            node['userState'] = []
    workflow.original['nodes'] = nodes
    workflow.state = False
    db_model.db.session.commit()



class CannotDecideActionException(Exception):
    pass


class MallforemedNet(Exception):
    pass
