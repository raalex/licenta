import json
from functools import total_ordering
from copy import deepcopy
from workflow_data import WorkflowData


@total_ordering
class PetriMarking:
    def __init__(self,
                 locations=None,
                 markings: "Dict of string -> integer" = None):
        if locations is None:
            if markings is None:
                self._markings = dict()
            else:
                self._markings = markings
        else:
            self._markings = dict.fromkeys(locations, 0)

    def generate_marking_from(root_marking: "PetriMarking",
                              workflow_data: WorkflowData,
                              transition):
        petri_marking = deepcopy(root_marking)
        archs_with_transition = workflow_data.get_arches_with_component(
            transition)
        if len(workflow_data.get_arches_to_component(transition)) == 0:
            raise IllegalMarkingException()
        for arch in archs_with_transition:
            if arch.get_from() == transition:
                petri_marking.increment_value_at_location(
                    arch.get_to(), arch.get_cost())
            else:
                petri_marking.decrement_value_at_location(
                    arch.get_from(), arch.get_cost())
        if petri_marking.are_all_zero():
            raise IllegalMarkingException("the data can be lost")
        return petri_marking

    def get_locations(self):
        return self._markings.keys()

    def are_all_zero(self):
        for value in self._markings:
            if value != 0:
                return False
        return True

    def get_markings(self):
        return self._markings

    def set_markings(self,
                     markings: "Dict of string -> integer"):
        self._markings = markings

    def change_location_value(self, location, value):
        self._markings[location] = value

    def get_value_of_location(self, location):
        return self._markings[location]

    def increment_value_at_location(self, location, value=1):
        self._markings[location] += value

    def decrement_value_at_location(self, location, value=1):
        if self._markings[location] >= value:
            self._markings[location] -= value
        else:
            raise IllegalMarkingException("the data can be lost")

    def __eq__(self, other):
        if type(other) is type(self):
            return self._markings == other._markings
        return NotImplemented

    def __ne__(self, other):
        if type(other) is type(self):
            return not self.__eq__(other)
        return NotImplemented

    def __gt__(self, other):
        gt = False
        if all(value == 0 for value in self._markings.values()):
            return False
        if all(value == 0 for value in other._markings.values()):
            return False
        if type(other) is type(self):
            if list(self._markings.keys()) == list(other._markings.keys()):
                for location in self._markings.keys():
                    if self._markings[location] < other._markings[location]:
                        return False
                    if self._markings[location] > other._markings[location]:
                        gt = True
                return gt
            else:
                return False
        else:
            return NotImplemented

    def __str__(self):
        return json.dumps(self._markings, sort_keys=True)

    def __hash__(self):
        return hash(str(self))

    def __copy__(self):
        cls = self.__class__
        result = cls.__new__(cls)
        result.__dict__.update(self.__dict__)
        return result

    def __deepcopy__(self, memo):
        cls = self.__class__
        result = cls.__new__(cls)
        memo[id(self)] = result
        for k, v in self.__dict__.items():
            setattr(result, k, deepcopy(v, memo))
        return result


class IllegalMarkingException(Exception):
    pass
