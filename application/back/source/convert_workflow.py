from copy import deepcopy


def find_edges_with_node(edges, node, direction: "to or from"):
    """finds edges in which node apears at direction ('to' or 'from')"""
    edges_with_node = []
    for edge in edges:
        if str(edge[direction]) == str(node):
            edges_with_node.append(edge)
    return edges_with_node


def remove_edges_from_workflow(workflow, edges):
    for edge in workflow['edges'][:]:
        if edge in edges:
            workflow['edges'].remove(edge)


def transform_and_split(petri_repr,
                        edges_to_node,
                        edges_from_node,
                        node,
                        nr_aux_nodes,
                        nr_aux_transitions):
    # TODO: verify to be at least 2 out transitiona
    # verify if there is only one node as input
    """if len(edges_to_node) != 1:
        raise WorkflowBadlyFormed((
            "Node id=" +
            str(node['id']) +
            " does not have one in node, it has " + str(len(edges_to_node))
        ))"""
    # add only the aux transition because the aux node
    # is added to the right of transitions
    transition_id = "aux_transition" + str(nr_aux_transitions)
    petri_repr['transitions'].append({
        "id": transition_id
    })
    nr_aux_transitions += 1
    petri_repr['edges'].append({
        "to": transition_id,
        "from": edges_to_node[0]['from']
    })
    # for each out transition add an aux node and connect them up together
    # and the node to the aux transition
    for index in range(len(edges_from_node)):
        """if "out" == edges_from_node[index]['to']:
            raise WorkflowBadlyFormed(
                "Output node must not be" +
                " connected to a split node"
            )"""
        current_id = "aux_node" + str(nr_aux_nodes)
        petri_repr['nodes'].append({
            "label": "Aux node " + str(nr_aux_nodes),
            "id": current_id
        })
        nr_aux_nodes += 1
        petri_repr['edges'].append({
            "to": edges_from_node[index]['to'],
            "from": current_id
        })
        petri_repr['edges'].append({
            "to": current_id,
            "from": transition_id
        })
    return nr_aux_nodes, nr_aux_transitions


def transform_and_join(petri_repr,
                       edges_to_node,
                       edges_from_node,
                       node,
                       nr_aux_nodes,
                       nr_aux_transitions):
    # TODO: verify that there are at leas 2 input nodes
    # verify if there is only one transition as output
    """if len(edges_from_node) != 1:
        raise WorkflowBadlyFormed((
            "Node id=" +
            str(node['id']) +
            " does not have one out node"
        ))"""
    # add the node and transition for the output, taking in consideration
    # that the original output node can be the workflows finnish
    current_home_node = "out"
    if edges_from_node[0]['to'] != "out":
        current_home_node = "aux_node" + str(nr_aux_nodes)
        petri_repr['nodes'].append({
            "label": "Aux node " + str(nr_aux_nodes),
            "id": current_home_node
        })
        nr_aux_nodes += 1
        petri_repr['edges'].append({
            "from": current_home_node,
            "to": edges_from_node[0]['to']
        })
    transition_id = "aux_transition" + str(nr_aux_transitions)
    petri_repr['transitions'].append({
        "id": transition_id
    })
    nr_aux_transitions += 1
    petri_repr['edges'].append({
        "from": transition_id,
        "to": current_home_node
    })
    # for each input node add edge from it to the transition added
    # earlier, because the nodes are added tot the right of a transition
    for index in range(len(edges_to_node)):
        petri_repr['edges'].append({
            "from": edges_to_node[index]['from'],
            "to": transition_id
        })
    return nr_aux_nodes, nr_aux_transitions


def transform_or_join(petri_repr,
                      edges_to_node,
                      edges_from_node,
                      node,
                      nr_aux_nodes,
                      nr_aux_transitions):
    # TODO: verify that there are at least 2 input nodes
    # verify that there is only one output transition
    """if len(edges_from_node) != 1:
        raise WorkflowBadlyFormed((
            "Node id=" +
            str(node['id']) +
            " does not have one out node"
        ))"""
    # add the output node taking in consideration
    # that the original output node can be the workflows finnish
    current_home_node = "out"
    if edges_from_node[0]['to'] != "out":
        current_home_node = "aux_node" + str(nr_aux_nodes)
        petri_repr['nodes'].append({
            "label": "Aux node " + str(nr_aux_nodes),
            "id": current_home_node
        })
        nr_aux_nodes += 1
        petri_repr['edges'].append({
            "from": current_home_node,
            "to": edges_from_node[0]['to']
        })
    # for each input node add a transition and edges to connect them up
    # together and the transition to the output node, because the
    # nodes are added to the right of transitions
    for index in range(len(edges_to_node)):
        transition_id = "aux_transition" + str(nr_aux_transitions)
        petri_repr['transitions'].append({
            "id": transition_id
        })
        nr_aux_transitions += 1
        petri_repr['edges'].append({
            "from": edges_to_node[index]['from'],
            "to": transition_id
        })
        petri_repr['edges'].append({
            "from": transition_id,
            "to": current_home_node
        })
    return nr_aux_nodes, nr_aux_transitions


def transform_or_split(petri_repr,
                       edges_to_node,
                       edges_from_node,
                       node,
                       nr_aux_nodes,
                       nr_aux_transitions):
    # TODO: verify that there at least 2 output transitions
    # verify that there is only 1 input node
    """if len(edges_to_node) != 1:
        raise WorkflowBadlyFormed((
            "Node id=" +
            str(node['id']) +
            " does not have one in node"
        ))"""
    # there is no need to add an aux node, because for each
    # transition a node is added to its right

    # for each output transition add an aux transition and node and
    # connect them up together, then the node to the out transition,
    # then the in node to the aux transition
    """for loc in petri_repr['nodes']:
        if loc['id'] == edges_to_node[0]['from']:
            loc['switchValues'] = node['switchValues']
            break"""
    for index in range(len(edges_from_node)):
        if "out" == edges_from_node[index]['to']:
            raise WorkflowBadlyFormed(
                "Output node must not be" +
                " connected to a split node"
            )
        current_id = "aux_node" + str(nr_aux_nodes)
        transition_id = "aux_transition" + str(nr_aux_transitions)
        petri_repr['transitions'].append({
            "id": transition_id
        })
        nr_aux_transitions += 1
        petri_repr['nodes'].append({
            "label": "Aux node " + str(nr_aux_nodes),
            "id": current_id
        })
        nr_aux_nodes += 1
        petri_repr['edges'].append({
            "to": edges_from_node[index]['to'],
            "from": current_id
        })
        petri_repr['edges'].append({
            "to": current_id,
            "from": transition_id
        })
        petri_repr['edges'].append({
            "to": transition_id,
            "from": edges_to_node[0]['from'],
            "label": edges_from_node[index]['to']
        })
    return nr_aux_nodes, nr_aux_transitions


def transform_action_edge(petri_repr,
                          edges_to_node,
                          edges_from_node,
                          node,
                          nr_aux_nodes,
                          nr_aux_transitions):
    # Verify if nodes have just one in and one out edge, taking in
    # consideration the workflows start and finish nodes
    """if node['id'] != 'in' and node['id'] != 'out':
        if len(edges_from_node) != 1 or len(edges_to_node) != 1:
            raise WorkflowBadlyFormed((
                "Node with label " +
                str(node['label']) +
                " must have one in node and one out node but has in:" +
                str(len(edges_to_node)) +
                " nodes and out:" +
                str(len(edges_from_node)) + " nodes"
            ))
    if node['id'] == 'in' and len(edges_from_node) != 1:
        raise WorkflowBadlyFormed((
            "Node id=" +
            str(node['id']) +
            " must have one out node but has in:" +
            str(len(edges_to_node)) +
            " and out:" +
            str(len(edges_from_node))
        ))
    if node['id'] == 'out' and len(edges_to_node) != 1:
        raise WorkflowBadlyFormed((
            "Node id=" +
            str(node['id']) +
            " must have one in node but has in:" +
            str(len(edges_to_node)) +
            " and out:" +
            str(len(edges_from_node))
        ))"""
    # add a node as output to the transition and update the edges
    if len(edges_from_node) != 0:
        if node['id'] != 'out' and edges_from_node[0]['to'] != 'out':
            current_id = "aux_node" + str(nr_aux_nodes)
            petri_repr['nodes'].append({
                "label": "Aux node " + str(nr_aux_nodes),
                "id": current_id
            })
            nr_aux_nodes += 1
            petri_repr['edges'].append({
                "to": current_id,
                "from": node['id']
            })
            petri_repr['edges'].append({
                "to": edges_from_node[0]['to'],
                "from": current_id
            })
        # if the out node of transition is the workflow finish then the
        # edge will not be removed from the original workflow
        if edges_from_node[0]['to'] == 'out':
            edges_from_node.pop()
    else:
        current_id = "aux_node" + str(nr_aux_nodes)
        petri_repr['nodes'].append({
            "label": "Aux node " + str(nr_aux_nodes),
            "id": current_id
        })
        nr_aux_nodes += 1
        petri_repr['edges'].append({
            "to": current_id,
            "from": node['id']
        })
    return nr_aux_nodes, nr_aux_transitions


def pop_in_out_nodes(petri_repr):
    # move the start and finish nodes from the transitions list and return them
    in_node = None
    out_node = None
    for node in petri_repr['transitions'][:]:
        if node['id'] == 'in':
            in_node = node
            petri_repr['transitions'].remove(node)
            continue
        if node['id'] == 'out':
            out_node = node
            petri_repr['transitions'].remove(node)
            continue
    return in_node, out_node


def convert_to_petri_representation(workflow):
    # copy the workflow and add the transitions list an reinitialize the
    # nodes list with only the start and finish nodes
    petri_repr = deepcopy(workflow)
    petri_repr['transitions'] = petri_repr['nodes']
    petri_repr['nodes'] = []
    petri_repr['nodes'] += pop_in_out_nodes(petri_repr)
    nr_aux_nodes = 0
    nr_aux_transitions = 0
    # for each transition chech it's type and do the coresponding
    # translation, after that remove the old edges from the list
    for node in petri_repr['transitions'][:]:
        if str(node['id']).find('aux_') == -1:
            edges_to_node = find_edges_with_node(
                petri_repr['edges'],
                node['id'],
                "to"
            )
            edges_from_node = find_edges_with_node(
                petri_repr['edges'],
                node['id'],
                "from"
            )
            if node['label'] == "And Join":
                nr_aux_nodes, nr_aux_transitions = transform_and_join(
                    petri_repr,
                    edges_to_node,
                    edges_from_node,
                    node,
                    nr_aux_nodes,
                    nr_aux_transitions
                )
                petri_repr['transitions'].remove(node)
                remove_edges_from_workflow(petri_repr, edges_to_node)
            elif node['label'] == "And Split":
                nr_aux_nodes, nr_aux_transitions = transform_and_split(
                    petri_repr,
                    edges_to_node,
                    edges_from_node,
                    node,
                    nr_aux_nodes,
                    nr_aux_transitions
                )
                petri_repr['transitions'].remove(node)
                remove_edges_from_workflow(petri_repr, edges_to_node)
            elif node['label'] == "Or Join":
                nr_aux_nodes, nr_aux_transitions = transform_or_join(
                    petri_repr,
                    edges_to_node,
                    edges_from_node,
                    node,
                    nr_aux_nodes,
                    nr_aux_transitions
                )
                petri_repr['transitions'].remove(node)
                remove_edges_from_workflow(petri_repr, edges_to_node)
            elif node['label'] == "Or Split":
                nr_aux_nodes, nr_aux_transitions = transform_or_split(
                    petri_repr,
                    edges_to_node,
                    edges_from_node,
                    node,
                    nr_aux_nodes,
                    nr_aux_transitions
                )
                petri_repr['transitions'].remove(node)
                remove_edges_from_workflow(petri_repr, edges_to_node)
            else:
                nr_aux_nodes, nr_aux_transitions = transform_action_edge(
                    petri_repr,
                    edges_to_node,
                    edges_from_node,
                    node,
                    nr_aux_nodes,
                    nr_aux_transitions
                )
            remove_edges_from_workflow(petri_repr, edges_from_node)
    return petri_repr


class WorkflowBadlyFormed(Exception):
    pass
