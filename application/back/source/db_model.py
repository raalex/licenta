from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects import postgresql
from sqlalchemy.ext.mutable import MutableDict, MutableList
from sqlalchemy.types import Enum
import psycopg2
import utility_functions
import enum

db = SQLAlchemy()


class JobType(enum.Enum):
    manager = "manager"
    developer = "developer"
    tester = "tester"
    secretary = "secretary"


class Employee(db.Model):
    __tablename__ = "employee"
    job_types = ["manager", "developer", "tester", "secretary"]
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    username = db.Column(db.Text, unique=True)
    name = db.Column(db.Text)
    password = db.Column(db.Text)
    salt = db.Column(db.Text)
    job = db.Column(Enum(JobType))
    employment_data = db.Column(db.Date)
    tasks = db.relationship('Task', backref='employee', lazy='dynamic')
    workflows = db.relationship('Workflow', backref='employee', lazy='dynamic')

    def __init__(self, username, name, password, job, emp_date):
        pass_data = utility_functions.get_hashed_password(password)
        self.username = username
        self.name = name
        self.password = pass_data[0]
        self.salt = pass_data[1]
        self.job = job
        self.employment_data = emp_date

    def __repr__(self):
        rep = {}
        rep['id'] = self.id
        return rep


class StateTypes(enum.Enum):
    to_do = "to_do"
    doing = "doing"
    done = "done"


class Task(db.Model):
    __tablename__ = "task"
    state_types = ["to_do", "doing", "done"]
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    employee_id = db.Column(db.Integer, db.ForeignKey('employee.id'))
    workflow_id = db.Column(db.Integer, db.ForeignKey('workflow.id'))
    action_id = db.Column(db.Text)
    title = db.Column(db.Text)
    description = db.Column(db.Text)
    response_values = db.Column(MutableDict.as_mutable(postgresql.JSON))
    response = db.Column(db.Text)
    in_resources = db.Column(MutableList.as_mutable(postgresql.ARRAY(db.Text, dimensions=1)))
    out_resources = db.Column(MutableList.as_mutable(postgresql.ARRAY(db.Text, dimensions=1)))
    state = db.Column(Enum(StateTypes))

    def __init__(
        self,
        employee_id,
        workflow_id,
        title,
        description,
        in_resources,
        out_resources,
        action_id,
        response_values
    ):
        self.employee_id = employee_id
        self.workflow_id = workflow_id
        self.title = title
        self.description = description
        self.in_resources = in_resources
        self.out_resources = out_resources
        self.state = "to_do"
        self.action_id = action_id
        self.response_values = response_values


class Workflow(db.Model):
    __tablename__ = "workflow"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    employee_id = db.Column(db.Integer, db.ForeignKey('employee.id'))
    name = db.Column(db.Text)
    state = db.Column(db.Boolean)
    original = db.Column(MutableDict.as_mutable(postgresql.JSON))
    petri_representation = db.Column(MutableDict.as_mutable(postgresql.JSON))
    current_marking = db.Column(MutableDict.as_mutable(postgresql.JSON))
    or_split_data = db.Column(MutableDict.as_mutable(postgresql.JSON))
    tasks = db.relationship('Task', backref='workflow', lazy='dynamic')

    def __init__(self, employee_id, name, original, petri_representation):
        self.employee_id = employee_id
        self.name = name
        self.original = original
        self.petri_representation = petri_representation
