app.controller('TaskController', ['$scope', 'TaskService', function($scope, taskService) {
	$scope.tasks = {};
	$scope.networks = [];
    $scope.currentWorkflow = null;
	taskService.getParticipatingWorkflows("1").then(function(data) {
		$scope.networks = data.workflows;
	});
    
	$scope.selectWorkflow = function(networkID) {
		//console.log("apelat pt " + networkID);
        $scope.currentWorkflow = networkID;
        taskService.getTasks("1", networkID).then(function(data) {
        	console.log(data);
            $scope.tasks.to_do = data.to_do;
            $scope.tasks.doing = data.doing;
            $scope.tasks.done = data.done;
            //console.log($scope);
        });
    };
}]);