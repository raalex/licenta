app.controller('WorkflowController', ['$scope', 'WorkflowService', 'VisDataSet', function($scope, workflowService, VisDataSet) {
    $scope.show = false;
    //$scope.selectedNode = null;
    workflowService.getWorkflowList("1").then(function(data) {
        $scope.networks = data.workflows;
        // console.log($scope.networks);
    });
    $scope.node_types = [{
        name: "Action",
        value: "action"
    }, {
        name: "Edge",
        value: "edge"
    }, {
        name: "Or Split",
        value: "or-split"
    }, {
        name: "Or Join",
        value: "or-join"
    }, {
        name: "And Split",
        value: "and-split"
    }, {
        name: "And Join",
        value: "and-join"
    }, {
        name: "Remove",
        value: "remove"
    }];
    $scope.job_types = [];
    $scope.selectedNode = null;

    var findUnnamedNodeNumber = function() {
        var nodes = $scope.workflowData.nodes.get();
        var number = 0;
        angular.forEach(nodes, function(value, key) {
            if (value.shape === "box") {
                number++;
            }
        });
        return number;
    };

    $scope.setWorkflowData = function(data) {
        if('error' in data) {
            alert(data.error);
        } else {
            var tempData = {
                nodes: VisDataSet(data.workflow.nodes),
                edges: VisDataSet(data.workflow.edges),
                id: data.id,
                name: data.name,
                state: data.state
            };
            $scope.job_types = data.user_type;
            $scope.workflowData = tempData;
            $scope.nodeNumber = findUnnamedNodeNumber();
            $scope.selectedNode = null;
            $scope.workflowState = data.state;
        }
    };

    $scope.createNewNetwork = function() {
        workflowService.createNewWorkflow("1").then(function(data) {
            $scope.setWorkflowData(data);
            $scope.networks.push({
                id: data.id,
                name: data.name
            });
        });
    };

    $scope.selectWorkflow = function(networkID) {
        workflowService.getWorkflow("1", networkID).then(function(data) {
            $scope.setWorkflowData(data);
        });
    };

    $scope.saveWorkflow = function() {
        var tempData = {
            workflow: {
                nodes: $scope.workflowData.nodes.get(),
                edges: $scope.workflowData.edges.get()
            },
            id: $scope.workflowData.id,
            name: $scope.workflowData.name
        };

        workflowService.updateWorkflow("1", tempData.id, tempData).then(function(data) {
            //$scope.networks[tempData.id] = tempData;
            if (data !== null) {
                alert(data.error);
            }
        });
    };

    $scope.deleteWorkflow = function() {
        var tempData = {
            workflow: {
                nodes: VisDataSet($scope.rawWorkflowData.nodes),
                edges: VisDataSet($scope.rawWorkflowData.edges)
            },
            id: undefined,
            name: undefined
        };

        workflowService.deleteWorkflow("1", $scope.workflowData.id).then(function(data) {
            angular.forEach($scope.networks, function(value, key) {
                if (value.id == $scope.workflowData.id) {
                    $scope.networks.splice(key, 1);
                }
            });
            $scope.workflowData = tempData;
        });
    };

    $scope.startWorkflow = function() {
        var tempData = {
            workflow: {
                nodes: $scope.workflowData.nodes.get(),
                edges: $scope.workflowData.edges.get()
            },
            id: $scope.workflowData.id,
            name: $scope.workflowData.name
        };

        workflowService.updateWorkflow("1", tempData.id, tempData).then(function(data) {
            if (data === null) {
                workflowService.startWorkflow("1", $scope.workflowData.id).then(function(data) {
                    $scope.setWorkflowData(data);
                });
            } else {
                alert(data.error);
            }
        });
    };

    $scope.interruptWorkflow = function() {
        workflowService.interruptWorkflow("1", $scope.workflowData.id).then(function(data) {
            $scope.setWorkflowData(data);
        });
    };

    $scope.runNode = function() {
        workflowService.runNextNode("1", $scope.workflowData.id, $scope.selectedNode.id).then(function(data) {
            $scope.setWorkflowData(data);
        });
    };

    $scope.updateNode = function() {
        //console.log($scope.selectedNode);
        $scope.workflowData.nodes.update($scope.selectedNode);
    };

    $scope.selectNextNode = function() {
        workflowService.runNextNode("1", $scope.workflowData.id, $scope.selectedNode.nextNode).then(function(data) {
            var tempData = {
                nodes: VisDataSet(data.workflow.nodes),
                edges: VisDataSet(data.workflow.edges),
                id: data.id,
                name: data.name,
                state: data.state
            };
            $scope.job_types = data.user_type;
            $scope.workflowData = tempData;
            $scope.nodeNumber = findUnnamedNodeNumber();
            $scope.selectedNode = null;
            $scope.workflowState = data.state;
        });
    };

    $scope.onSelect = function(event) {
        if (event.nodes.length !== 0) {
            var node = $scope.workflowData.nodes.get(event.nodes[0]);
            if (node.allow_selection === true) {
                $scope.orSplit = node.label === "Or Split";
                $scope.selectedNode = node;
                $scope.$apply();
            }
        }
    };

    $scope.onClick = function(event) {
        if ($scope.element_type === "remove" && event.nodes.indexOf("in") === -1 && event.nodes.indexOf("out") === -1 && $scope.workflowData.state !== true) {
            $scope.workflowData.nodes.remove(event.nodes);
            $scope.workflowData.edges.remove(event.edges);
        } else {
            if (event.nodes.length === 0 && event.edges.length === 0 && $scope.element_type !== "edge" && $scope.element_type !== undefined && $scope.workflowData.state !== true) {
                var node = {
                    x: event.pointer.canvas.x,
                    y: event.pointer.canvas.y,
                    shape: "ellipse",
                    allow_selection: false,
                    color: "#90CAF9",
                    fixed: {
                        x: false,
                        y: false
                    }
                };

                switch ($scope.element_type) {
                    case "action":
                        $scope.nodeNumber += 1;
                        node.id = String($scope.nodeNumber);
                        node.label = "Node " + $scope.nodeNumber;
                        node.shape = "box";
                        node.allow_selection = true;
                        node.employees = [];
                        node.inResources = [];
                        node.outResources = [];
                        node.color = "#EEEEEE";
                        break;
                    case "or-join":
                        node.label = "Or Join";
                        break;
                    case "or-split":
                        node.label = "Or Split";
                        //node.allow_selection = true;
                        break;
                    case "and-join":
                        node.label = "And Join";
                        break;
                    case "and-split":
                        node.label = "And Split";
                        break;
                }
                $scope.workflowData.nodes.add(node);
            }

            if ($scope.element_type === "edge" && event.nodes.length !== 0 && $scope.workflowData.state !== true) {
                //console.log("click");
                if (startEdge === null) {
                    startEdge = event.nodes[0];
                } else {
                    var edge = {
                        from: startEdge,
                        to: event.nodes[0],
                        color: "grey"
                    };
                    $scope.workflowData.edges.add(edge);
                    startEdge = null;
                }
            } else {
                startEdge = null;
            }
        }
    };

    $scope.workflow_options = {
        physics: {
            // enabled: false,
            stabilization: true
        },
        edges: {
            arrows: {
                to: {
                    enabled: true,
                    scaleFactor: 1,
                    type: 'arrow'
                }
            }
        },
        nodes: {
            fixed: {
                x: false,
                y: false
            }
        }
    };

    $scope.events = {
        click: $scope.onClick,
        selectNode: $scope.onSelect
    };

    $scope.rawWorkflowData = {
        nodes: [],
        edges: []
    };

    $scope.workflowData = {
        nodes: VisDataSet($scope.rawWorkflowData.nodes),
        edges: VisDataSet($scope.rawWorkflowData.edges)
    };
}]);