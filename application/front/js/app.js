var app = angular.module('StartApp', ['ngRoute', 'ngMaterial', 'ngVis']);
app.config(function($routeProvider, $locationProvider, $mdThemingProvider) {
	// $locationProvider.html5Mode({
	// 	enabled: true,
	// 	requireBase: false
	// })
	$locationProvider.hashPrefix("");

	$routeProvider
		.when("/", {
			controller: "TaskController",
			templateUrl: "views/task.html"
		})
		.when("/workflow", {
			controller: "WorkflowController",
			templateUrl: "views/workflow.html"
		})
		.otherwise({
			redirectTo: "/"
		});

	$mdThemingProvider.theme('light-blue').backgroundPalette('light-blue', {
		'default': '200'
	});
});

var baseAPIurl = "http://localhost:5000/";
