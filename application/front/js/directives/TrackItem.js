app.directive("trackItem", ['TaskService', function(taskService) {
	return {
		restrict: "E",
		replace: false,
		scope: "=",
		templateUrl: "templates/TrackItem.html",
		link: function($scope, element, attr) {
			$scope.progress = function(event) {
				response = $scope.task.response;
				if($scope.task.state === 'to_do' || $scope.task.response === undefined) {
					response = null;
				}
				taskService.advanceTask("1", $scope.currentWorkflow, $scope.task.id, response).then(function(data){
					$scope.tasks.to_do = data.to_do;
					$scope.tasks.doing = data.doing;
					$scope.tasks.done = data.done;
				});
			};
		}
	};
}]);