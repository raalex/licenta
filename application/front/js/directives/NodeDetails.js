app.directive("nodeDetails",[function() {
    return {
        restrict: "E",
        replace: true,
        scope: "=",
        link: function($scope, element, attr) {
            $scope.$watch('workflowData', function(newValue) {
                $scope.templateFile = "templates/EditingNode.html";
                if(newValue.state === true) {
                    $scope.templateFile = "templates/RunningNode.html";
                }
                console.log("template file");
                console.log($scope.templateFile);
            }, true);
        },
        template: '<div ng-include="templateFile" flex ng-class="{invisible: selectedNode == null}" class="scrool"></div>'
    };
}]);