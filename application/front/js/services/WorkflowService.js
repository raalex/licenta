app.factory('WorkflowService', ['$http', function($http) {
	return {
		createNewWorkflow: function(userID) {
			return $http.post(baseAPIurl + 'users/' + userID + '/workflows')
				.then(function(success) {
					return success.data;
				}, function(error) {
					return error.data;
				});
		},
		getWorkflowList: function(userID) {
			return $http.get(baseAPIurl + 'users/' + userID + '/workflows')
				.then(function(success) {
					console.log(success);
					return success.data;
				}, function(error) {
					console.log(error);
					return error.data;
				});
		},
		getWorkflow: function(userID, workflowID) {
			return $http.get(baseAPIurl + 'users/' + userID + '/workflows/' + workflowID)
				.then(function(success) {
					console.log(success);
					return success.data;
				}, function(error) {
					console.log(error);
					return error.data;
				});
		},
		updateWorkflow: function(userID, workflowID, workflowData) {
			return $http.post(
					baseAPIurl + 'users/' + userID + '/workflows/' + workflowID,
					JSON.stringify({
						workflow: workflowData
					})
				)
				.then(function(success) {
					return success.data;
				}, function(error) {
					return error.data;
				});
		},
		startWorkflow: function(userID, workflowID) {
			return $http.post(
					baseAPIurl + 'users/' + userID + '/workflows/' + workflowID + '/start'
				)
				.then(function(success) {
					return success.data;
				}, function(error) {
					return error.data;
				});
		},
		deleteWorkflow: function(userID, workflowID) {
			return $http.delete(
					baseAPIurl + 'users/' + userID + '/workflows/' + workflowID
				).then(function(success) {
					return success.data;
				}, function(error) {
					return error.data;
				});
		},
		runNextNode: function(userID, workflowID, nextNodeID) {
			return $http.post(
				baseAPIurl + "users/" + userID + "/next-action/" + workflowID + "/" + nextNodeID
				).then(function(success) {
					return success.data;
				}, function(error) {
					return error.data;
				});
		},
		interruptWorkflow: function(userID, workflowID) {
			return $http.post(
					baseAPIurl + 'users/' + userID + '/workflows/' + workflowID + '/interrupt'
				)
				.then(function(success) {
					return success.data;
				}, function(error) {
					return error.data;
				});
		}
	};
}]);