app.factory('TaskService', ['$http', function($http) {
    return {
        advanceTask: function(userID, workflowID, taskID, taskResponse) {
            return $http.post(
                baseAPIurl + 'users/' + userID + '/tasks/' + workflowID + '/' + taskID,
                JSON.stringify({
                        response: taskResponse
                    })
                )
                .then(function(success) {
                    console.log(success);
                    return success.data;
                }, function(error) {
                    return error.data;
                });
        },
        getTasks: function(userID, workflowID) {
            return $http.get(baseAPIurl + 'users/' + userID + '/tasks/' + workflowID)
                .then(function(success) {
                    return success.data;
                }, function(error) {
                    return error.data;
                });
        },
        getParticipatingWorkflows: function(userID) {
            return $http.get(baseAPIurl + 'users/' + userID + '/tasks')
                .then(function(success) {
                    return success.data;
                }, function(error) {
                    return error.data;
                });
        }
    };
}]);